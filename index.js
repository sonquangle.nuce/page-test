(function () {
  var throttle = function (type, name, obj) {
    var obj = obj || window;
    var running = false;
    var func = function () {
      if (running) {
        return;
      }
      running = true;
      requestAnimationFrame(function () {
        obj.dispatchEvent(new CustomEvent(name));
        running = false;
      });
    };
    obj.addEventListener(type, func);
  };
  throttle("scroll", "optimizedScroll");
})();

var leftgear = document.getElementById("leftgear"), leftgear_02 = document.getElementById("leftgear_02"),
  rightgear = document.getElementById("rightgear");

// to use the script *without* anti-jank, set the event to "scroll" and remove the anonymous function.

window.addEventListener("optimizedScroll", function () {
  leftgear.style.transform = "rotate(" + window.pageYOffset / 40 + "deg)";
  leftgear_02.style.transform = "rotate(" + window.pageYOffset / 40 + "deg)";
  rightgear.style.transform = "rotate(-" + window.pageYOffset / 40 + "deg)";
});

$("#change_img").mouseover(function () {
  $("#change_img").addClass("image7");
  setTimeout(function () {
    $("#change_img").attr("src", "images/khung 1/2.png").removeClass("image7");
  }, 1900);
})
$("#change_img").mouseout(function () {
  $("#change_img").addClass("image7");
  setTimeout(function () {
    $("#change_img").attr("src", "images/khung 1/1.png").removeClass("image7");
  }, 1900);
});

$("#gallery_left").on("mouseover",function () {
  $("#gallery_event_left").addClass("progress-ican--child").removeClass("progress-ican--out");
  
});
$("#gallery_left").on('mouseout',function () {
  $("#gallery_event_left").addClass("progress-ican--out").removeClass("progress-ican--child");
});

$("#gallery_right").mouseover(function () {
  $("#gallery_event_right").addClass("progress-ican--child").removeClass("progress-ican--out");
  
})
$("#gallery_right").mouseout(function () {
  $("#gallery_event_right").addClass("progress-ican--out").removeClass("progress-ican--child");
});

$("#gallery_product").mouseover(function () {
  $("#gallery_product_left").addClass("progress-ican--child").removeClass("progress-ican--out");
  
})
$("#gallery_product").mouseout(function () {
  $("#gallery_product_left").addClass("progress-ican--out").removeClass("progress-ican--child");
});

$("#video_product").mouseover(function () {
  $("#gallery_product_right").addClass("progress-ican--child").removeClass("progress-ican--out");
  
})
$("#video_product").mouseout(function () {
  $("#gallery_product_right").addClass("progress-ican--out").removeClass("progress-ican--child");
});
